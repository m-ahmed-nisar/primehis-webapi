﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace PrimeHIS.Utils
{
    public class DataManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession Session => _httpContextAccessor.HttpContext.Session;

        private readonly IConfiguration _config;
        private string _compName;

        public DataManager(IConfiguration config, IHttpContextAccessor httpContextAccessor)
        {
            _config = config;
            _httpContextAccessor = httpContextAccessor;
        }


        #region NewConnection

        private SqlConnection New_connection(string compName = null)
        {
            if (string.IsNullOrEmpty(Session.GetString("compName")) || compName != null)
            {
                _compName = string.IsNullOrEmpty(compName) ? "ind" : compName;
                Session.SetString("compName", _compName);
            }
            else
            {
                _compName = Session.GetString("compName");
            }

            var key = _config["Key"];
            var connStr = Cryptography.Decrypt(_config.GetConnectionString(_compName), key);
            var source = connStr + ";" + "Connect Timeout=0";
            var conn = new SqlConnection(source);
            return conn;
        }

        #endregion


        #region Dataset

        public DataSet GetDataSet(string spName, IEnumerable<ViewParam> par, string compName = null)
        {
            var ds = new DataSet();
            var con = New_connection(compName);
            var cmd = new SqlCommand(spName, con) {CommandType = CommandType.StoredProcedure, CommandTimeout = 0};
            foreach (var vp in par) cmd.Parameters.AddWithValue("@" + vp.Name.Trim(), vp.Value);
            var da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            return ds;
        }

        public DataTable Get(string query, IEnumerable<ViewParam> par, string compName = null)
        {
            var dt = new DataTable();
            using var con = New_connection(compName);
            var cmd = new SqlCommand(query, con) {CommandType = CommandType.Text, CommandTimeout = 0};
            foreach (var vp in par) cmd.Parameters.AddWithValue("@" + vp.Name.Trim(), vp.Value);
            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            return dt;
        }

        public DataTable Get(string query, string compName = null)
        {
            var dt = new DataTable();
            using var con = New_connection(compName);
            var cmd = new SqlCommand(query, con) {CommandType = CommandType.Text, CommandTimeout = 0};
            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            return dt;
        }

        public string GetValue(string query, IEnumerable<ViewParam> par, string compName = null)
        {
            using var con = New_connection(compName);
            var dt = new DataTable();
            var cmd = new SqlCommand(query, con) {CommandType = CommandType.Text, CommandTimeout = 0};
            foreach (var vp in par) cmd.Parameters.AddWithValue("@" + vp.Name.Trim(), vp.Value);
            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        public string GetValue(string query, string compName = null)
        {
            using var con = New_connection(compName);
            var dt = new DataTable();
            var cmd = new SqlCommand(query, con) {CommandType = CommandType.Text, CommandTimeout = 0};

            var da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }


        public string GetScalar(string query, string compName = null)
        {
            using var con = New_connection(compName);
            var cmd = new SqlCommand(query, con) {CommandType = CommandType.Text, CommandTimeout = 0};
            var value = "";
            try
            {
                con.Open();
                value = (string) cmd.ExecuteScalar();
            }
            catch
            {
                value = "";
            }
            finally
            {
                con.Close();
            }

            return value;
        }

        #endregion
    }
}