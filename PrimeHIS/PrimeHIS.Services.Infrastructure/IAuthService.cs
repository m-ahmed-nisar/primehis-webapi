﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrimeHIS.Dto;
using PrimeHIS.Dto.Auth;

namespace PrimeHIS.Services.Infrastructure
{
    public interface IAuthService
    {
        (AuthenticateResponse, int) Authenticate(AuthenticateRequest model, string ipAddress);
        AuthenticateResponse GetCurrentUser(int id);
        public bool ValidateLogin(string authKey, int userId);
        bool ValidateRole(string roles, string path);
    }
}