﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrimeHIS.Dto;
using PrimeHIS.Dto.Auth;
using PrimeHIS.Helper.JWT;
using PrimeHIS.Services.Infrastructure;
using PrimeHIS.Utils;

namespace PrimeHIS.Controllers
{
    public class CipherController : BaseController
    {
        #region ActionMethod

        // [Authorize]
        [HttpGet]
        public string GetEncryption(string value, string passkey)
        {
            return Cryptography.Encrypt(value, passkey);
        }

        // [Authorize]
        [HttpGet]
        public string GetDecryption(string value, string passkey)
        {
            return Cryptography.Decrypt(value, passkey);
        }

        #endregion
    }
}