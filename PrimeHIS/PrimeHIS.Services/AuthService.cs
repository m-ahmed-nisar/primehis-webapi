﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PrimeHIS.Dto;
using PrimeHIS.Helper;
using PrimeHIS.Helper.JWT;
using PrimeHIS.Services.Infrastructure;
using PrimeHIS.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using PrimeHIS.Dto.Auth;

namespace PrimeHIS.Services
{
    public class AuthService : IAuthService
    {
        #region ClassMembers

        private DataManager _dataManager;
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession Session => _httpContextAccessor.HttpContext.Session;
        private DataTable _dataTable = new();
        private DataSet _dataSet = new();
        private TResponse _response = new();
        private List<ViewParam> _list = new();

        #endregion

        #region Constructor

        public AuthService(IOptions<AppSettings> appSettings, IConfiguration config,
            IHttpContextAccessor httpContextAccessor)
        {
            _appSettings = appSettings.Value;
            _dataManager = new DataManager(config, httpContextAccessor);
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region  Services
        public (AuthenticateResponse, int) Authenticate(AuthenticateRequest model, string ipAddress)
        {
            var authenticateResponse = new AuthenticateResponse();
            var retVal = 0;
            const string query =
                "select Usr_kid,  Usr_Code,Usr_Passkey, Usr_Password,Usr_SuccessLoginTime,  Usr_FailedAttempt,isnull( Usr_FailedAttemptdatetime, DATEADD(MINUTE,-30,GETDATE())) Usr_FailedAttemptdatetime,Usr_AuthKey, Usr_SingleSession from H_Usr where Usr_Status=1 and Usr_Code=@userCode";
            _list.Add(new ViewParam() {Name = "userCode", Value = model.UserCode});
            _dataTable = _dataManager.Get(query, _list, model.CompName);

            if (_dataTable != null)
            {
                if (_dataTable.Rows.Count > 0)
                {
                    var attempt = Convert.ToInt32(_dataTable.Rows[0]["Usr_FailedAttempt"].ToString());
                    var failedTime = Convert.ToDateTime(_dataTable.Rows[0]["Usr_FailedAttemptdatetime"].ToString());
                    var id = _dataTable.Rows[0]["Usr_kid"].ToString();
                    if (attempt <= 3 || failedTime < DateTime.Now.AddMinutes(-15))
                    {
                        if (string.Equals(
                            Cryptography.Encrypt(model.Password, _dataTable.Rows[0]["Usr_Passkey"].ToString()),
                            _dataTable.Rows[0]["Usr_Password"].ToString()))
                        {
                            var singleSession = Convert.ToBoolean(_dataTable.Rows[0]["Usr_SingleSession"].ToString());
                            var authKey = _dataTable.Rows[0]["Usr_AuthKey"].ToString();
                            if (singleSession != true || authKey == "" || model.LogIn == true)
                            {
                                authKey = Cryptography.GetKey();
                                var lastLogin = _dataTable.Rows[0]["Usr_SuccessLoginTime"].ToString();
                                _list.Clear();
                                _list.Add(new ViewParam() {Name = "usrId", Value = id});
                                _list.Add(new ViewParam() {Name = "ipAddress", Value = ipAddress});
                                _list.Add(new ViewParam() {Name = "authkey", Value = authKey});
                                _dataSet = _dataManager.GetDataSet("H_LoginUserSuccess", _list);
                                if (_dataSet != null && _dataSet.Tables.Count > 0)
                                {
                                    var roles = _dataSet.Tables[0].Rows[0]["Roles"].ToString();
                                    authenticateResponse = _dataSet.Tables[0].DataTableToList<AuthenticateResponse>().FirstOrDefault();
                                    if (authenticateResponse != null)
                                    {
                                        authenticateResponse.LastLogin = lastLogin == "" ? (DateTime?) null : Convert.ToDateTime(lastLogin);
                                        authenticateResponse.Token = GenerateJwtToken(id, authKey, roles);
                                        Session.SetObject<AuthenticateResponse>("User", authenticateResponse);
                                    }

                                    retVal = 1; //return 200 true --->   User login success;
                                }
                                else
                                {
                                    retVal = -1; //return 501  -->  Error occured
                                }
                            }
                            else
                            {
                                retVal = 2; //Return 200-false ---->User already login  
                            }
                        }
                        else
                        {
                            _list.Clear();
                            _list.Add(new ViewParam() {Name = "usrId", Value = id});
                            _list.Add(new ViewParam() {Name = "ipAddress", Value = ipAddress});
                            _list.Add(new ViewParam() {Name = "description", Value = "Incorrect password count " + (attempt + 1).ToString() + "."});
                            _dataSet = _dataManager.GetDataSet("H_LoginUserFailed", _list);
                            if (_dataSet != null && _dataSet.Tables.Count > 0)
                            {
                                retVal = 3; //Return 401-true ---->  User login failed 
                            }
                        }
                    }
                    else
                    {
                        _list.Clear();
                        _list.Add(new ViewParam() {Name = "usrId", Value = id});
                        _list.Add(new ViewParam() {Name = "ipAddress", Value = ipAddress});
                        _list.Add(new ViewParam() {Name = "description", Value = "Incorrect password count " + (attempt + 1).ToString() + "."});
                        _dataSet = _dataManager.GetDataSet("H_LoginUserFailed", _list);
                        if (_dataSet != null && _dataSet.Tables.Count > 0)
                        {
                            retVal = 4; //Return 401-false -->  max unsuccessful login reached 
                        }
                    }
                }
                else
                {
                    retVal = 0; //Return 403 User not found..
                }
            }
            else
            {
                retVal = -1; //Return Error if any Response->500
            }

            return (authenticateResponse, retVal);
        }
        #endregion

        #region GetOrValidateCurrentUser

        public AuthenticateResponse GetCurrentUser(int id)
        {
            const string query = "select user_kid as Id ,user_code UserCode,usr_fname FirstName,usr_mname MiddleName,usr_lname LastName,usr_pcontact Contact,usr_pemail Email,usr_lastlogin LastLogin, '' as Roles from h_usr where user_kid=@id";;
            _list.Clear();
            _list.Add(new ViewParam() {Name = "id", Value =id});
            _dataTable = _dataManager.Get(query, _list);
            return _dataTable.DataTableToList<AuthenticateResponse>().FirstOrDefault();
        }
        public bool ValidateLogin(string authKey, int userId)
        {
            const string query = "select count(Usr_kid) from  h_usr where Usr_kid=@usrId and (Usr_AuthKey=@authKey or Usr_SingleSession=0) and Usr_Status=1";
            _list.Add(new ViewParam(){Name = "usrId", Value = userId});
            _list.Add(new ViewParam(){Name = "authKey", Value = authKey});
            var count = _dataManager.GetValue(query, _list);
            return Convert.ToInt32(count) > 0;
        }
        public bool ValidateRole(string roles, string path)
        {
            var count = _dataManager.GetScalar("");
            return Convert.ToInt32(count) > 0;
        }

        #endregion

        #region GenerateJwt
        /// <summary>
        /// //here role is comma separated  role id i.e.  if role is Admin, HR having ids 1 & 2  then roles will be 1,2  
        /// </summary>
        /// <param name="id"></param>
        /// <param name="authKey"></param>
        /// <param name="roles"></param>
        /// <returns></returns>
        private string GenerateJwtToken(string id, string authKey, string roles = "")
        {
            // generate token that is valid for 8 hrs only.
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _appSettings.Issuer,
                IssuedAt = DateTime.Now,
                Subject = new ClaimsIdentity(new[] {new Claim("id", id), new Claim("roles", roles), new Claim("authKey", authKey)}),
                Expires = DateTime.UtcNow.AddHours(_appSettings.AccessTokenExpiration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        #endregion
    }
}